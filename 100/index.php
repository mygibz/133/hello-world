<?php
    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if ($_POST['name'] != "" && $_POST['email'] != "" && $_POST['message'] != "") {
            $pdo = new PDO('mysql:host=127.0.0.1;dbname=moduleDB', 'test', 'test');
            $pdo->prepare("INSERT INTO onehundred VALUES (NULL,?,?,?,?)")->execute([$_POST['name'],strip_tags($_POST['email']),$_POST['message'],date("Y-m-d")]);
        } else die("Not all values are entered");
    } else if (isset($_GET['del'])) {
        $pdo = new PDO('mysql:host=127.0.0.1;dbname=moduleDB', 'test', 'test');
        $pdo->prepare("DELETE FROM onehundred WHERE id=?")->execute([$_GET['del']]);
        echo "<script>window.history.pushState('', '100', '.');</script>";
    }    
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>100</title>
        <link rel="stylesheet" href="style.css">
    </head>
    <body>
        <div id="wrapper">
            <!-- Table of things in it -->
            <table>
                <?php
                    $pdo = new PDO('mysql:host=127.0.0.1;dbname=moduleDB', 'test', 'test');
                
                    foreach ($pdo->query("SELECT * FROM onehundred") as $row) {
                        echo "<tr>";
                            echo "<td>" . $row['id'] . "</td>";
                            echo "<td>" . htmlspecialchars($row['name']) . "</td>";
                            echo "<td><a href='mailto:" . $row['email'] . "'>" . $row['email'] . "</a></td>";
                            echo "<td>" . htmlspecialchars($row['message']) . "</td>";
                            echo "<td>" . date('l jS \of F Y h:i:s A', strtotime($row['datum'])) . "</td>";
                            echo "<td><a href='index.php?del=" . $row['id'] . "'>&#x1f5d1;</a></td>";
                        echo "</tr>";
                    }
                ?>
            </table>

            <div style="height:50px"></div>

            <form action="index.php" method="post">
                <input type="text" name="name" placeholder="Name" required>
                <input type="email" name="email" placeholder="Email" required>
                <textarea name="message" cols="30" rows="10" placeholder="Message" required></textarea>
                <input type="submit" value="Add to database" style="width: 318px">
            </form>
        </div>
    </body>
</html>