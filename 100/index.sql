use moduleDB;

CREATE TABLE onehundred (
    id int NOT NULL AUTO_INCREMENT,
    name varchar(64),
    email varchar(128),
    message varchar(1028),
    datum datetime,
    PRIMARY KEY (id)
); 

INSERT INTO onehundred VALUES (1, 'Yanik', 'Ammann', 'Hallo', '2020-07-07');
INSERT INTO onehundred VALUES (2, 'Gay', 'Gayness', 'Gay', '1969-06-21');