<!DOCTYPE html>
<html>
  <head>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"  media="screen,projection"/>
    <style>
      a {
        color: unset;
      }
      .material-icons {
          margin-right: 5px;
      }
    </style>
  </head>

  <body>

    <nav class="blue-grey lighten-2">
        <div class="nav-wrapper">
            <a href="#" class="brand-logo center">It's <?php echo date("H:i"); ?></a>
        </div>
    </nav>

    <ul class="collection">
      <?php
        $sortedData = array();
        $folderToScan = ".";
        foreach(scandir($folderToScan) as $file) 
          if($file != '.' && $file != '..')
            if (!is_dir("$folderToScan/$file"))
              array_push($sortedData, "<li class=\"collection-item\" style='display: flex;'><i class='material-icons'>insert_drive_file</i> <a href=\"$folderToScan/$file\">$file</a></li>"); // add to end
            else
              array_unshift($sortedData, "<li class=\"collection-item\" style='display: flex;'><i class='material-icons'>folder</i> <a href=\"$folderToScan/$file\"><b>$file</b></a></li>"); // add to beginning
        foreach ($sortedData as $out)
          echo $out;
      ?>
    </ul>

    <span style="margin-left:10px">Your lucky number this turn: <?php echo rand(1,49) ?></div>

    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
  </body>
</html>